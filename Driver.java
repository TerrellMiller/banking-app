package app;

import java.util.ArrayList;
import java.util.Scanner;


public class Driver {
    public static void main(String[] args)
    {
        ArrayList<Customer> customers = new ArrayList<>();
        ArrayList<Employee> employees = new ArrayList<>();
        ArrayList<Administrator> administrators = new ArrayList<>();
        ArrayList<Account> accounts = new ArrayList<>();

        Scanner scan = new Scanner(System.in);

        customers.add(new Customer("Paul" , "djfa", "Ike", "n", "k@gmail.com"));
        customers.add(new Customer("Terrell" , "Miller", "tman", "g", "k@gmail.com"));
        employees.add(new Employee("g", "a", "p", "y", "t@gmail.com"));
        administrators.add(new Administrator("g", "a", "z", "x", "t@gmail.com"));

        System.out.println("Welcome to Terrell's Banking App");

        boolean inProgram = true;
        while(inProgram) {
            System.out.println("Please choose who you are by selecting a number: \n1. Customer\n2. Employee\n3. Administrator\nSelect any other number to End Program");
            String input = scan.next();
            try {
                int choice = Integer.parseInt(input);
                if (choice == 1)
                {
                    System.out.println("Customer: Choose from the following by selecting a number: \n1. login\n2. Create an application.");
                    input = scan.next();
                    choice = Integer.parseInt(input);
                    if (choice == 1)
                    {
                        System.out.println("Username: ");
                        input = scan.next();
                        System.out.println("Password: ");
                        String input2 = scan.next();
                        boolean found = false;
                        boolean customerOperations;

                        for (int i = 0; i < customers.size(); i++)
                        {
                            if (customers.get(i).getUsername().equals(input) && customers.get(i).getPassword().equals(input2)) {
                                found = true;
                                if (!customers.get(i).isApproval())
                                {
                                    System.out.println("Logging out, wait for your account to be approved then login.");

                                }
                                else{
                                    System.out.println("Here are your operations, you will be able to logout anytime.");
                                    customerOperations = true;

                                    for(int b = 0; b < accounts.size(); b++)
                                    {
                                        if(accounts.get(b).getUsername().equals(customers.get(b).getUsername()))
                                        {

                                            while(customerOperations)
                                            {
                                                System.out.println("Current Balance: " + accounts.get(b).getBalance());
                                                System.out.println("Choose from the following operations\n1. withdraw\n2. transfer\n3. deposit\n4. Select any other number to Logout");
                                                input = scan.next();
                                                try {
                                                    int choiceTwo = Integer.parseInt(input);
                                                    if(choiceTwo == 1)
                                                    {
                                                        accounts.get(b).withdraw();
                                                    }
                                                    else if(choiceTwo == 2)
                                                    {
                                                        System.out.println("Please Enter the Username of the account you want to transfer money to");
                                                        input = scan.next();
                                                        accounts.get(b).transfer(accounts, input);
                                                    }
                                                    else if(choiceTwo == 3)
                                                    {
                                                        accounts.get(b).deposit();
                                                    }
                                                    else
                                                    {
                                                        System.out.println("Logging out");
                                                        customerOperations = false;
                                                    }
                                                }
                                                catch(Exception g)
                                                {
                                                    System.out.println("Please Enter a number. The question will be asked again!\n");
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                         }
                        if(!found) {
                            System.out.println("Invalid information check your username and password");
                        }
                    }
                    else
                    {
                        System.out.println("Create an application with all the information. ");
                        System.out.println("Enter firstname");
                        input = scan.next();
                        String info1 = input;
                        System.out.println("Enter lastname");
                        input = scan.next();
                        String info2 = input;

                        boolean sameName = true;

                        while(sameName)
                        {
                            System.out.println("Enter username");
                            input = scan.next();

                            for(int i = 0; i < customers.size(); i++)
                            {
                                if(customers.get(i).getUsername().equals(input))
                                {
                                    sameName = true;
                                    System.out.println("Username is the same as another customer.");
                                }
                                else
                                    sameName = false;
                            }
                        }
                        String info3 = input;
                        System.out.println("Enter password");
                        input = scan.next();
                        String info4 = input;
                        System.out.println("Enter email");
                        input = scan.next();
                        String info5 = input;

                        Customer newCustomer = new Customer(info1, info2, info3, info4, info5);
                        customers.add(newCustomer);

                        System.out.println("Logging out, wait for your account to be approved then login.");
                    }
                }
                else if (choice == 2)
                {
                    System.out.println("Employee: Choose from the following by selecting a number: \n1. login\n2. Create an account.");
                    input = scan.next();
                    choice = Integer.parseInt(input);
                    if (choice == 1)
                    {
                        System.out.println("Username: ");
                        input = scan.next();
                        System.out.println("Password: ");
                        String input2 = scan.next();
                        Employee x = new Employee();
                        boolean found = false;
                        boolean employeeOperations = false;

                        for(int i = 0; i < employees.size(); i++)
                        {
                            if (employees.get(i).getUsername().equals(input) && employees.get(i).getPassword().equals(input2)) {
                                employeeOperations = true;
                                found = true;
                                System.out.println("Here are your operations, you will be able to logout anytime.");
                            }
                        }

                        while(employeeOperations)
                        {
                            System.out.println("Choose from the following operations\n1. View customers\n2. Approve customer applications\nSelect any other number to Logout");
                            input = scan.next();
                            try {
                                int choiceTwo = Integer.parseInt(input);
                                if(choiceTwo == 1)
                                {
                                    x.view(customers);
                                }
                                else if(choiceTwo == 2)
                                {
                                    System.out.println("Enter the username of the account.");
                                    String p = scan.next();
                                    x.approve(customers, accounts, p);
                                }
                                else
                                {
                                    System.out.println("Logging out");
                                    employeeOperations = false;
                                }
                            }
                            catch(Exception g)
                            {
                                System.out.println("Please Enter a number. The question will be asked again!\n");
                            }
                        }
                        if (!found) {
                            System.out.println("Invalid employee credentials.");
                        }
                    }
                    else
                    {
                        System.out.println("Create an employee account with all the information. ");
                        System.out.println("Enter firstname");
                        input = scan.next();
                        String info1 = input;
                        System.out.println("Enter lastname");
                        input = scan.next();
                        String info2 = input;

                        boolean sameName = true;

                        while(sameName)
                        {
                            System.out.println("Enter username");
                            input = scan.next();

                            for(int i = 0; i < employees.size(); i++)
                            {
                                if(employees.get(i).getUsername().equals(input))
                                {
                                    sameName = true;
                                    System.out.println("Username is the same as another customer.");
                                }
                                else
                                    sameName = false;
                            }

                        }

                        String info3 = input;
                        System.out.println("Enter password");
                        input = scan.next();
                        String info4 = input;
                        System.out.println("Enter email");
                        input = scan.next();
                        String info5 = input;

                        Employee newEmployee = new Employee(info1, info2, info3, info4, info5);
                        employees.add(newEmployee);

                        System.out.println("Logging out. Please login to access processes");
                    }
                }
                else if (choice == 3)
                {
                    System.out.println("Administrator: Choose one by selecting a number:\n1. login\n2. Create an Admin account.");
                    input = scan.next();
                    choice = Integer.parseInt(input);
                    if (choice == 1)
                    {
                        System.out.println("Username: ");
                        input = scan.next();
                        System.out.println("Password: ");
                        String input2 = scan.next();
                        Administrator x = new Administrator();
                        boolean found = false;
                        boolean adminOperations = false;

                        for(int i = 0; i < administrators.size(); i++)
                        {
                            if (administrators.get(i).getUsername().equals(input) && administrators.get(i).getPassword().equals(input2)) {
                                adminOperations = true;
                                found = true;
                                System.out.println("Here are your operations, you will be able to logout anytime.");
                            }
                        }
                        while(adminOperations)
                        {
                            System.out.println("Choose from the following operations\n1. View accounts\n2. Deny accounts\nSelect any other number to Logout");
                            input = scan.next();
                            try {
                                int choiceTwo = Integer.parseInt(input);
                                if(choiceTwo == 1)
                                {
                                    x.view(accounts);
                                }
                                else if(choiceTwo == 2)
                                {
                                    System.out.println("Enter the username of the account.");
                                    String p = scan.next();
                                    x.deny(accounts, customers, p);
                                }
                                else
                                {
                                    System.out.println("Logging out");
                                    adminOperations = false;
                                }
                            }
                            catch(Exception g)
                            {
                                System.out.println("Please Enter a number. The question will be asked again!\n");
                            }
                        }
                        if(!found) {
                            System.out.println("Invalid admin Credentials");
                        }
                    }
                    else
                    {
                        System.out.println("Create an administrator account with all the information. ");
                        System.out.println("Enter firstname");
                        input = scan.next();
                        String info1 = input;
                        System.out.println("Enter lastname");
                        input = scan.next();
                        String info2 = input;


                        boolean sameName = true;

                        while(sameName)
                        {
                            System.out.println("Enter username");
                            input = scan.next();

                            for(int i = 0; i < administrators.size(); i++)
                            {
                                if(administrators.get(i).getUsername().equals(input))
                                {
                                    sameName = true;
                                    System.out.println("Username is the same as another customer.");
                                }
                                else
                                    sameName = false;
                            }

                        }

                        String info3 = input;
                        System.out.println("Enter password");
                        input = scan.next();
                        String info4 = input;
                        System.out.println("Enter email");
                        input = scan.next();
                        String info5 = input;

                        Administrator newAdmin = new Administrator(info1, info2, info3, info4, info5);
                        administrators.add(newAdmin);

                        System.out.println("Logging out. Please login to access processes");
                    }
                }
                else
                {
                    System.out.println("Program Ended");
                    inProgram = false;
                }
            } catch (Exception e) {
                System.out.println("Please Enter a number. The question will be asked again!\n");
            }

        }
    }
}
