package app;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;

public class CustomerDAO {

    static Connection conn = JDBC_Connection.getInstance();

    public void updateCustomer(Customer cust) throws SQLException
    {
        String query = "insert into CUSTOMER(username," + "password) VALUES (?, ?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, cust.getUsername());
        ps.setString(2, cust.getPassword());
        ps.executeQuery();
        ps.executeUpdate();
    }

    public void deleteCustomer(String cust) throws SQLException
    {
        String query = "delete from customer where username = ?";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.executeUpdate();
    }

    public ArrayList<Customer> getAllCustomers() throws SQLException
    {
        String query = "select * from Customer";
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        ArrayList<Customer> customers = new ArrayList<>();

        while(rs.next())
        {
            Customer cust = new Customer();
            cust.setUsername(rs.getString("username"));
            cust.setPassword(rs.getString("password"));
            customers.add(cust);
        }

        return customers;
    }

    public void viewCustomers() throws SQLException
    {
        ArrayList<Customer> viewCustomers = getAllCustomers();

        for(int i = 0; i < viewCustomers.size(); i++)
        {
            System.out.println(viewCustomers.get(i));
        }
    }

    public boolean check(String username, String password) throws SQLException
    {
        boolean found = false;

        ArrayList<Customer> viewCustomers = getAllCustomers();

        for(int i = 0; i < viewCustomers.size(); i++)
        {
            if(viewCustomers.get(i).getUsername().equals(username) && viewCustomers.get(i).getPassword().equals(password))
            {
                found = true;
            }
        }

        if(!found)
        {
            System.out.println("Invalid Information, check your username and password");
        }

        return found;
    }
}
