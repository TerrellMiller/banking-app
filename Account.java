package app;

import java.util.ArrayList;
import java.util.Scanner;

public class Account {
    private double balance;
    private boolean joint;
    private boolean status;
    private String username;
    private String usernameTwo;
    private double LIMIT = 10000.00;


    Account() {
    }

    Account(boolean joint) {
        this.joint = joint;
        this.status = true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUsernameTwo(String usernameTwo) {
        this.usernameTwo = usernameTwo;
    }

    public String getUsername() {
        return username;
    }

    public String getUsernameTwo() {
        return usernameTwo;
    }

    public void setLIMIT(double LIMIT) {
        this.LIMIT = LIMIT;
    }


    /**
     * returns if it is a joint account
     */
    public boolean isJoint() {
        return joint;
    }

    /**
     * returns the status of the account
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * sets the status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * returns the balance
     */
    public double getBalance() {
        return balance;
    }

    /**
     * returns the daily limit
     */
    public double getLIMIT() {
        return LIMIT;
    }

    /**
     * sets the balance
     */
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /**
     * sets if the account is joint
     */
    public void setJoint(boolean joint) {
        this.joint = joint;
    }


    /**
     * cannot withdraw negative numbers
     * cannot withdraw more than what is in the account
     */
    public void withdraw() {
        System.out.println("How much would you like to withdraw?");
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        try {
            double num = Double.parseDouble(s);

            if (num < 0) {
                System.out.println("Number is negative please enter a positive number.");
            } else if (num > getBalance()) {
                System.out.println("Cannot withdraw more than what the account balance is.");
            } else if (num > getLIMIT()) {
                System.out.println("Cannot withdraw more than the daily limit");
            } else {
                setBalance(getBalance() - num);
                setLIMIT(getLIMIT() - num);
                System.out.println("Receipt: Your current balance is: " + getBalance() + " after withdrawing " + num);
            }

        } catch (NumberFormatException n) {
            System.out.println("Wrong input, please insert a number in the format of ##.##");
        }

    }

    /**
     * cannot transfer negative numbers
     * check if the account is canceled
     */
    public void transfer(ArrayList<Account> a, String b) {
        Scanner scan = new Scanner(System.in);

        boolean found = false;

        for (int i = 0; i < a.size(); i++) {

            if (a.get(i).getUsername().equals(b)) {
                found = true;
                System.out.println("How much would you like to transfer?");
                String s = scan.next();
                try {
                    double num = Double.parseDouble(s);
                    if (num < 0) {
                        System.out.println("Number is negative please enter a positive number.");
                    } else if (num > getBalance()) {
                        System.out.println("Cannot withdraw more than what the account balance is.");
                    } else if (num > getLIMIT()) {
                        System.out.println("Cannot withdraw more than the daily limit");
                    } else
                    {
                        setBalance(getBalance() - num);
                        setLIMIT(getLIMIT() - num);
                        a.get(i).setBalance(a.get(i).getBalance() + num);
                        System.out.println("Receipt: Your current balance is: " + getBalance() + " after transferring "  + num);
                    }
                } catch (NumberFormatException n) {
                    System.out.println("Input invalid, please insert a number in the format of ##.##");
                }
            }
        }

        if(!found)
        {
            System.out.println("Account was not found");
        }


    }
    /**
     * A method to override the object's toString where if the account is joint it also list the name
     * of the second user
     */
    @Override
    public String toString() {
        String r = "Account Info: " +
                "balance= " + balance +
                ", joint=" + joint +
                ", status=" + status +
                ", username=" + username + '\'';

        if (isJoint()) {
            r.concat("second user " + usernameTwo);
        }

        return r;
    }

    //cannot deposit negative numbers
    public void deposit() {
        System.out.println("How much would you like to deposit?");
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        try {
            double num = Double.parseDouble(s);

            if (num < 0) {
                System.out.println("Number is negative please enter a positive number.");
            } else {
                setBalance(getBalance() + num);
                System.out.println("Receipt: Your current balance is: " + getBalance() + " after depositing " + num);
            }
        } catch (NumberFormatException n) {
            System.out.println("Input invalid, please insert a number in the format of ##.##");
        }
    }
}

