package app;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Employee extends Customer{

    private String workerID;

    Employee()
    {}

    public Employee(String firstName, String lastName, String username, String password, String email)
    {
        setEmail(email);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        setUsername(username);
        setWorkerID();
    }

    public void setWorkerID() {
        Random r = new Random();
        int id = r.nextInt(9999) + 1000;
        this.workerID = Integer.toString(id);
    }

    public String getWorkerID() {
        return workerID;
    }

    public void view(ArrayList c)
    {
        System.out.println("\n" + c);
    }

    /**
     * Creates an account for a customer
     * supports joint account
     * can add another customer who has a Customer application open
     * */
    public Account createAccount(ArrayList<Customer> x, ArrayList<Account> y)
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Is the account joint, type in Yes, yes, y, or Y if it is");
        String input = scan.next();
        boolean joint = false;
        boolean found = false;
        boolean approval = false;

        String secondPerson = "";

        //input for joint account
        if(input.equalsIgnoreCase("yes") || input.equalsIgnoreCase("y"))
        {
            System.out.println("Please enter the username of the other person.");
            input = scan.next();
            //checks customer database for the inputted username
            for (int i = 0; i < x.size(); i++) {

                if (x.get(i).getUsername().equals(input))
                {
                    found = true;
                }

                if(x.get(i).isApproval())
                {
                    approval = true;
                }
            }

            if(found && approval == false)
            {
                joint = true;
            }
        }

            //create account for customer
            Account a = new Account(joint);

            //adds the second username to the account if it is joint and does not have approval

            if(a.isJoint())
            {
                a.setUsernameTwo(secondPerson);
                System.out.println("Successfully added " + a.getUsernameTwo());
            }

            y.add(a);
            return a;
    }

    @Override
    public String toString() {
        return "\nEmployee info: + firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", username='" + getUsername() + '\'' +
                ", password='" + getPassword() + '\'' +
                ", email='" + getEmail() + '\'' +
                '}';
    }

    /**
     * approves a customer by searching for their username given by the employee
     * This method checks the username and approves the customer
     * Create account is then called to create an account for the customer by using their username
     * */
    public void approve(ArrayList<Customer> x, ArrayList<Account> y, String name) {

        for (int i = 0; i < x.size(); i++) {
            if (x.get(i).getUsername().equals(name)) {
                if (x.get(i).isApproval() == false) {
                    x.get(i).setApproval(true);
                    System.out.println(x.get(i).getUsername() + "\nAccount Has been approved");
                    createAccount(x, y).setUsername(name);
                }

            }
        }
    }
}
