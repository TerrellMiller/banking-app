package app;

import java.util.ArrayList;

public class Administrator extends Employee
{

    public Administrator()
    {}

    public Administrator(String firstName, String lastName, String username, String password, String email)
    {
        setEmail(email);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        setUsername(username);
        setWorkerID();
    }


    @Override
    public String toString() {
        return "\nAdministrator info: " + "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", username='" + getUsername() + '\'' +
                ", password='" + getPassword() + '\'' +
                ", email='" + getEmail() + '\'' +
                '}';
    }

    public void deny(ArrayList<Account> x, ArrayList<Customer> y, String name)
    {
        for(int i = 0; i < x.size(); i++)
        {
            if(x.get(i).getUsername().equals(name))
            {
                System.out.println("Deny the user account: " + name);
                for(int j = 0; j < y.size(); j++)
                {
                    if(y.get(j).getUsername().equals(name))
                    {
                        y.get(j).setApproval(false);
                        System.out.println(name + " no longer has approval, will need to create another application.");
                    }
                }

                if(x.get(i).isJoint())
                {
                    for(int s = 0; s < y.size(); s++)
                    {
                        if(y.get(s).getUsername().equals(x.get(i).getUsernameTwo()))
                        {
                            y.get(s).setApproval(false);
                            System.out.println(y.get(s).getUsername() + " no longer has approval, will need to create another application.");
                        }
                    }
                }
                x.get(i).setStatus(false);
                System.out.println("Status of the account is now inactive.");
            }
        }
    }
}
