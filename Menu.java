package app;

import java.sql.SQLException;
import java.util.Scanner;


public class Menu {
    public static Scanner scan = new Scanner(System.in);
    public static Scanner scanner = new Scanner((System.in));

    public static void start() {

        JDBC_Connection conn = new JDBC_Connection();
        CustomerDAO customerDAO = new CustomerDAO();
        AdministratorDAO administratorDAO = new AdministratorDAO();
        EmployeeDAO employeeDAO = new EmployeeDAO();
        AccountDAO accountDAO = new AccountDAO();
        ApplicationDAO applicationDAO = new ApplicationDAO();
        TransactionDAO transactionDAO = new TransactionDAO();


        boolean inMenu = true;
        while(inMenu) {
            System.out.println("Please choose who you are by selecting a number: \n1. Customer\n2. Employee\n3. Administrator\nSelect any other number to End Program");
            String input = scan.next();
            try {
                int choice = Integer.parseInt(input);
                if (choice == 1)
                {
                    System.out.println("Customer: Choose from the following by selecting a number: \n1. login\n2. Create an application.");
                    input = scan.next();
                    choice = Integer.parseInt(input);
                    if (choice == 1)
                    {
                        System.out.println("Username: ");
                        String inputOne = scan.next();
                        System.out.println("Password: ");
                        String inputTwo = scan.next();

                        boolean customerOperations = false;


                        if(customerDAO.check(inputOne, inputTwo))
                        {
                            customerOperations = true;
                        }

                        while(customerOperations)
                        {
                            System.out.println("Choose from the following operations\n1. withdraw\n2. transfer\n3. deposit\nSelect any other number to Logout");
                            input = scan.next();
                            try {
                                int choiceTwo = Integer.parseInt(input);
                                if(choiceTwo == 1)
                                {
                                    System.out.println("Choice 1");
                                }
                                else if(choiceTwo == 2)
                                {
                                    System.out.println("Choice 2");

                                }
                                else if(choiceTwo == 3)
                                {
                                    System.out.println("Choice 3");
                                }
                                else
                                {
                                    System.out.println("Logging out");
                                    customerOperations = false;
                                }
                            }
                            catch(Exception g)
                            {
                                System.out.println("Please Enter a number. The question will be asked again!\n");
                            }
                        }
                    }
                    else
                    {
                        System.out.println("Username: ");
                        input = scan.next();
                        System.out.println("Password: ");
                        String input2 = scan.next();

                        Customer c = new Customer(input, input2);
                        customerDAO.updateCustomer(c);

                        /*Application app = new Application(c.getUsername());
                        applicationDAO.updateApplication(app);*/

                        customerDAO.viewCustomers();

                    }
                }
                else if (choice == 2)
                {
                    System.out.println("Employee: Choose from the following by selecting a number: \n1. login\n2. Create an account." + " TEST");


                    /*input = scan.next();
                    choice = Integer.parseInt(input);
                    if (choice == 1)
                    {
                        System.out.println("Username: ");
                        input = scan.next();
                        System.out.println("Password: ");
                        String input2 = scan.next();
                        Employee x = new Employee();
                        boolean found = false;
                        boolean employeeOperations = false;

                        for(int i = 0; i < employees.size(); i++)
                        {
                            if (employees.get(i).getUsername().equals(input) && employees.get(i).getPassword().equals(input2)) {
                                employeeOperations = true;
                                found = true;
                                System.out.println("Here are your operations, you will be able to logout anytime.");
                            }
                        }

                        while(employeeOperations)
                        {
                            System.out.println("Choose from the following operations\n1. View customers\n2. Approve customer applications\nSelect any other number to Logout");
                            input = scan.next();
                            try {
                                int choiceTwo = Integer.parseInt(input);
                                if(choiceTwo == 1)
                                {
                                    x.view(customers);
                                }
                                else if(choiceTwo == 2)
                                {
                                    System.out.println("Enter the username of the account.");
                                    String p = scan.next();
                                    x.approve(customers, accounts, p);
                                }
                                else
                                {
                                    System.out.println("Logging out");
                                    employeeOperations = false;
                                }
                            }
                            catch(Exception g)
                            {
                                System.out.println("Please Enter a number. The question will be asked again!\n");
                            }
                        }
                        if (!found) {
                            System.out.println("Invalid employee credentials.");
                        }
                    }
                    else
                    {
                        System.out.println("Create an employee account with all the information. ");

                        System.out.println("Logging out. Please login to access processes");
                    }*/
                }
                else if (choice == 3)
                {
                    System.out.println("Administrator: Choose one by selecting a number:\n1. login\n2. Create an Admin account." + " TEST");
                    /*input = scan.next();
                    choice = Integer.parseInt(input);
                    if (choice == 1)
                    {
                        System.out.println("Username: ");
                        input = scan.next();
                        System.out.println("Password: ");
                        String input2 = scan.next();
                        Administrator x = new Administrator();
                        boolean found = false;
                        boolean adminOperations = false;

                        for(int i = 0; i < administrators.size(); i++)
                        {
                            if (administrators.get(i).getUsername().equals(input) && administrators.get(i).getPassword().equals(input2)) {
                                adminOperations = true;
                                found = true;
                                System.out.println("Here are your operations, you will be able to logout anytime.");
                            }
                        }
                        while(adminOperations)
                        {
                            System.out.println("Choose from the following operations\n1. View accounts\n2. Deny accounts\nSelect any other number to Logout");
                            input = scan.next();
                            try {
                                int choiceTwo = Integer.parseInt(input);
                                if(choiceTwo == 1)
                                {
                                    x.view(accounts);
                                }
                                else if(choiceTwo == 2)
                                {
                                    System.out.println("Enter the username of the account.");
                                    String p = scan.next();
                                    x.deny(accounts, customers, p);
                                }
                                else
                                {
                                    System.out.println("Logging out");
                                    adminOperations = false;
                                }
                            }
                            catch(Exception g)
                            {
                                System.out.println("Please Enter a number. The question will be asked again!\n");
                            }
                        }
                        if(!found) {
                            System.out.println("Invalid admin Credentials");
                        }
                    }
                    else
                    {
                        System.out.println("Create an administrator account with all the information. ");
                        System.out.println("Enter firstname");
                        input = scan.next();
                        String info1 = input;
                        System.out.println("Enter lastname");
                        input = scan.next();
                        String info2 = input;


                        System.out.println("Enter username");
                        input = scan.next();
                        String info3 = input;
                        System.out.println("Enter password");
                        input = scan.next();
                        String info4 = input;
                        System.out.println("Enter email");
                        input = scan.next();
                        String info5 = input;

                        Administrator newAdmin = new Administrator(info1, info2, info3, info4, info5);
                        administrators.add(newAdmin);

                        System.out.println("Logging out. Please login to access processes");
                    }*/
                }
                else
                {
                    System.out.println("Program Ended");
                    inMenu = false;
                }
            }
            catch(java.sql.SQLIntegrityConstraintViolationException username)
            {
                System.out.println("Username is not unique. Failed to complete application.");
            }
            catch(SQLException p)
            {
                System.out.println("this is an sql exception");
                p.printStackTrace();
            }
            catch (Exception e) {
               // System.out.println("Please Enter a number. The question will be asked again!\n");

                System.out.println("exception part");
                e.printStackTrace();
            }

        }




    }

}