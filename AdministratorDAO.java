package app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AdministratorDAO {

    static Connection conn = JDBC_Connection.getInstance();

    public void updateAdministrator(Administrator administrator) throws SQLException
    {
        String query = "insert into Customer(username," + "password) VALUES (?, ?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, administrator.getUsername());
        ps.setString(2, administrator.getPassword());
        ps.executeQuery();
        ps.executeUpdate();
    }

    public void deleteAdministrator(Administrator administrator) throws SQLException
    {
        String query = "insert into Administrator(username," + "password) VALUES (?, ?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, administrator.getUsername());
        ps.setString(2, administrator.getPassword());
        ps.executeQuery();
        ps.executeUpdate();
    }

    public ArrayList<Administrator> getAllAdministrators() throws SQLException
    {
        String query = "select * from Administrators";
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        ArrayList<Administrator> administrators = new ArrayList<>();

        while(rs.next())
        {
            Administrator administrator = new Administrator();
            administrator.setUsername(rs.getString("username"));
            administrator.setPassword(rs.getString("password"));
            administrators.add(administrator);
        }

        return administrators;
    }
}
