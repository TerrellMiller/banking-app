package app;

import java.sql.*;

public class JDBC_Connection {

    private static Connection conn = null;

    static final String user = "admin";
    static final String password = "12345678";
    static final String db_url = "jdbc:oracle:thin:@java-react.cvtq9j4axrge.us-east-1.rds.amazonaws.com:1521:ORCL";

    public JDBC_Connection()
    {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection(db_url, user, password);
        }
        catch(ClassNotFoundException a)
        {
            System.out.println("Unable to load driver class");

        } catch (SQLException b) {
            System.out.println("Something is wrong with your SQL");
        }
    }

    public static Connection getInstance()
    {
            return conn;
    }
}
